﻿using Coscine.ApiCommons;
using Coscine.Database.Models;
using Coscine.Database.ReturnObjects;
using Coscine.Database.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Coscine.Api.ActivatedFeatures.Controllers
{
    /// <summary>
    /// This controller represents the actions which can be taken with a feature object.
    /// </summary>
    [Authorize]
    public class ActivatedFeaturesController : Controller
    {
        private readonly ActivatedFeaturesModel _activatedFeaturesModel;
        private readonly FeaturesModel _featuresModel;
        private readonly Authenticator _authenticator;
        private readonly ProjectModel _projectModel;

        /// <summary>
        /// ActivatedFeaturesController constructor specifying an ActivatedFeaturesModel, a FeaturesModel, an authenticator and a ProjectMode.
        /// </summary>
        public ActivatedFeaturesController()
        {
            _activatedFeaturesModel = new ActivatedFeaturesModel();
            _featuresModel = new FeaturesModel();
            _authenticator = new Authenticator(this, Program.Configuration);
            _projectModel = new ProjectModel();
        }

        /// <summary>
        /// Returns all available features.
        /// </summary>
        /// <returns>Features</returns>
        [HttpGet("[controller]")]
        public ActionResult<List<FeatureObject>> GetFeatures()
        {
            var features = _featuresModel.GetAll().Select(x => new FeatureObject(x, false)).ToList();
            return Json(features);
        }

        /// <summary>
        /// Returns features of the project.
        /// </summary>
        /// <param name="projectId">Project for which the features are returned</param>
        /// <returns>Features or StatusCode 401</returns>
        [HttpGet("[controller]/{projectId}")]
        public ActionResult<List<FeatureObject>> GetFeatures(string projectId)
        {
            var user = _authenticator.GetUser();
            var project = _projectModel.GetById(Guid.Parse(projectId));

            if (_projectModel.HasAccess(user, project, UserRoles.Owner, UserRoles.Member))
            {
                var features = _activatedFeaturesModel.GetActiveFeatures(project.Id).Select(x => new FeatureObject(x, true)).ToList();
                features.AddRange(_activatedFeaturesModel.GetInactiveFeatures(project.Id).Select(x => new FeatureObject(x, false)).ToList());
                return Json(features);
            }
            else
            {
                return Forbid("User has no Access to this project");
            }
        }
        /// <summary>
        /// Returns activated features of the project.
        /// </summary>
        /// <param name="projectId">Project for which the activated features are returned</param>
        /// <returns>ActiveFeatures or StatusCode 401</returns>
        [HttpGet("[controller]/{projectId}/activeFeatures")]
        public ActionResult<List<FeatureObject>> GetActiveFeatures(string projectId)
        {
            var user = _authenticator.GetUser();
            var project = _projectModel.GetById(Guid.Parse(projectId));

            if (_projectModel.HasAccess(user, project, UserRoles.Owner, UserRoles.Member))
            {
                return Json(_activatedFeaturesModel.GetActiveFeatures(project.Id).Select(x => new FeatureObject(x, true)).ToList());
            }
            else
            {
                return Forbid("User has no Access to this project");
            }
        }

        /// <summary>
        /// Returns the inactive features of the project.
        /// </summary>
        /// <param name="projectId">Project for which the inactivate features are returned</param>
        /// <returns>InactiveFeatures or StatusCode 401</returns>
        [HttpGet("[controller]/{projectId}/inactiveFeatures")]
        public ActionResult<List<FeatureObject>> GetInactiveFeatures(string projectId)
        {
            var user = _authenticator.GetUser();
            var project = _projectModel.GetById(Guid.Parse(projectId));

            if (_projectModel.HasAccess(user, project, UserRoles.Owner, UserRoles.Member))
            {
                return Json(_activatedFeaturesModel.GetInactiveFeatures(project.Id).Select(x => new FeatureObject(x, false)).ToList());
            }
            else
            {
                return Forbid("User has no Access to this project");
            }
        }

        /// <summary>
        /// Activates the feature of the project.
        /// </summary>
        /// <param name="projectId">Project for which the feature is activated</param>
        /// <param name="featureId">Feature</param>
        /// <returns>StatusCode 204 or 401</returns>
        [HttpGet("[controller]/{projectId}/activateFeature/{featureId}")]
        public IActionResult ActivateFeature(string projectId, string featureId)
        {
            var user = _authenticator.GetUser();
            var project = _projectModel.GetById(Guid.Parse(projectId));
            var feature = _featuresModel.GetById(Guid.Parse(featureId));

            if (_projectModel.HasAccess(user, project, UserRoles.Owner))
            {
                _activatedFeaturesModel.ActivateFeature(project.Id, feature.Id);
                return NoContent();
            }
            else
            {
                return Forbid("User has no Access to this project");
            }
        }

        /// <summary>
        /// Deactives the feature of the project.
        /// </summary>
        /// <param name="projectId">Project for which the feature is deactivated</param>
        /// <param name="featureId">Feature</param>
        /// <returns>StatusCode 204 or 401</returns>
        [HttpGet("[controller]/{projectId}/deactivateFeature/{featureId}")]
        public IActionResult DeactivateFeature(string projectId, string featureId)
        {
            var user = _authenticator.GetUser();
            var project = _projectModel.GetById(Guid.Parse(projectId));
            var feature = _featuresModel.GetById(Guid.Parse(featureId));

            if (_projectModel.HasAccess(user, project, UserRoles.Owner))
            {
                _activatedFeaturesModel.DeactivateFeature(project.Id, feature.Id);
                return NoContent();
            }
            else
            {
                return Forbid("User has no Access to this project");
            }
        }

    }
}
